package server.rekening;

import java.util.*;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.*;

/**
 *
 * @author jakbar
 */
@WebService(serviceName = "DetailRekening")
public class DetailRekening {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs = null;
    ConnectionMySQL connectSQL = new ConnectionMySQL();

    @WebMethod(operationName = "detail")
    public Rekening detail(@WebParam(name = "nomor_rekening") String rek) {
        String nama = "";
        int saldo = 0;
        String nomor = "";
        int id_rekening = -1;
        ArrayList<Transaksi> T = new ArrayList<Transaksi>();
        Rekening rekening = new Rekening("", "", 0, T);

        try {
            conn = connectSQL.getConnect();
            pstm = conn.prepareStatement("SELECT * "
                + "FROM rekening "
                + "WHERE nomor_rekening = " + rek);
            rs = pstm.executeQuery();
            while (rs.next()) {
                nama = rs.getString("nama");
                saldo = rs.getInt("saldo");
                nomor = rs.getString("nomor_rekening");
                id_rekening = rs.getInt("id_rekening");
            }

            conn = connectSQL.getConnect();
            pstm = conn.prepareStatement("select * from transaksi "
                + "where idRekening = " + String.valueOf(id_rekening));
            rs = pstm.executeQuery();

            while (rs.next()) {
                String tujuan = rs.getString("tujuan");
                String jenis = rs.getString("jenis");
                String waktu = rs.getString("waktuTransaksi");
                int nominal = rs.getInt("nominal");

                Transaksi tr = new Transaksi(tujuan, waktu, jenis, nominal);
                T.add(tr);
            }

            rekening = new Rekening(nama, nomor, saldo, T);

        } catch (Exception e) {
            System.out.println("Failed to Show :" + e.toString());
        }

        return rekening;

    }
}
