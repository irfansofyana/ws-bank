package server.rekening;

import java.util.*;

/**
 *
 * @author jakbar
 */
public class Rekening {
    private String nama;
    private String nomor_rekening;
    private int saldo;
    private ArrayList<Transaksi> transaksi;

    public Rekening(
        String nama,
        String nomor_rekening,
        int saldo,
        ArrayList<Transaksi> transaksi
    ) {
        this.nama = nama;
        this.nomor_rekening = nomor_rekening;
        this.saldo = saldo;
        this.transaksi = transaksi;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNomor_rekening() {
        return nomor_rekening;
    }

    public void setNomor_rekening(String nomor_rekening) {
        this.nomor_rekening = nomor_rekening;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public ArrayList<Transaksi> getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(ArrayList<Transaksi> transaksi) {
        this.transaksi = transaksi;
    }
}
