package server.rekening;

import java.util.*;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.*;

/**
 *
 * @author x550
 */
@WebService(serviceName = "DoTransaksi")
public class DoTransaksi {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs = null;
    ConnectionMySQL connectSQL = new ConnectionMySQL();

    @WebMethod(operationName = "transaction")
    public boolean doTransaksi(
        @WebParam(name = "pengirim") String pengirim,
        @WebParam(name = "penerima") String penerima,
        @WebParam(name = "nominal") int nominal
    ) {
        boolean isValid = false;

        try {
            ValidateRekening rekeningPengirim = new ValidateRekening();
            ValidateRekening rekeningPenerima = new ValidateRekening();
            int idPengirim = rekeningPengirim.validate(pengirim);
            int idPenerima = rekeningPenerima.validate(penerima);

            if (idPengirim == -1 || idPenerima == -1) {
                throw new Error();
            }

            conn = connectSQL.getConnect();
            pstm = conn.prepareStatement("SELECT * "
                + "FROM rekening "
                + "WHERE id_rekening = " + idPengirim);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int saldoPengirim = rs.getInt("saldo");
                if (saldoPengirim >= nominal) {
                    isValid = true;
                }
            }

            if (isValid) {
                pstm = conn.prepareStatement("UPDATE rekening "
                    + "SET saldo = saldo - " + String.valueOf(nominal)
                    + " WHERE id_rekening = " + String.valueOf(idPengirim));
                pstm.executeUpdate();

                pstm = conn.prepareStatement("UPDATE rekening "
                    + "SET saldo = saldo + " + String.valueOf(nominal)
                    + " WHERE id_rekening = " + String.valueOf(idPenerima));
                pstm.executeUpdate();

                pstm = conn.prepareStatement("INSERT INTO transaksi "
                    + "(idRekening, jenis, nominal, tujuan)"
                    + " VALUE (?,?,?,?)");

                pstm.setInt(1, idPengirim);
                pstm.setString(2, "KREDIT");
                pstm.setInt(3, nominal);
                pstm.setString(4, penerima);
                pstm.executeUpdate();

                pstm = conn.prepareStatement("INSERT INTO transaksi "
                    + "(idRekening, jenis, nominal, tujuan)"
                    + " VALUE (?,?,?,?)");
                pstm.setInt(1, idPenerima);
                pstm.setString(2, "DEBIT");
                pstm.setInt(3, nominal);
                pstm.setString(4, pengirim);
                pstm.executeUpdate();
            }
        } catch (Exception er) {
            System.out.println("Failed to do transaction: " + er.toString());
        }
       return isValid;
    }
}
