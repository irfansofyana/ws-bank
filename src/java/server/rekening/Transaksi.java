package server.rekening;

/**
 *
 * @author jakbar
 */
public class Transaksi {
    private String tujuan;
    private String waktu;
    private String jenis;
    private int nominal;

    public Transaksi(String tujuan, String waktu, String jenis, int nominal) {
        this.tujuan = tujuan;
        this.waktu = waktu;
        this.jenis = jenis;
        this.nominal = nominal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public int getNominal() {
        return nominal;
    }

    public void setNominal(int nominal) {
        this.nominal = nominal;
    }
}
