package server.rekening;

import java.util.*;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.*;

/**
 *
 * @author jakbar
 */
@WebService(serviceName = "ValidateRekening")
public class ValidateRekening {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs = null;
    ConnectionMySQL connectSQL = new ConnectionMySQL();

    @WebMethod(operationName = "validate")
    public int validate(@WebParam(name = "nomor_rekening") String rek) {
        boolean valid = false;
        int id_rekening = -1;

        try {
            conn = connectSQL.getConnect();
            pstm = conn.prepareStatement("SEELCT * "
                + "FROM rekening "
                + "WHERE nomor_rekening = " + rek);
            rs = pstm.executeQuery();
            while (rs.next()) {
                id_rekening = rs.getInt("id_rekening");
                valid = true;
            }

            if (!valid) {
                conn = connectSQL.getConnect();
                pstm = conn.prepareStatement("SELECT * "
                    + "FROM virtual_rekening "
                    + "WHERE nomor_virtual = " + rek);
                rs = pstm.executeQuery();
                while (rs.next()) {
                    id_rekening = rs.getInt("id_rekening");
                    valid = true;
                }
            }

        } catch (Exception e) {
            System.out.println("Failed to Show :" + e.toString());
        }

        return id_rekening;
    }
}
