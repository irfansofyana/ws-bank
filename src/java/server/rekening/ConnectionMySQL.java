package server.rekening;

import java.sql.*;

/**
 *
 * @author jakbar
 */
public class ConnectionMySQL {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs = null;

    public Connection getConnect() {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String JDBC_URL = "jdbc:mysql://localhost:3306/bank-pro";
        String USER = "root";
        String PASS = "";

        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(JDBC_URL, USER, PASS);
        } catch (Exception e) {
            System.out.println("Connection Fail" + e.toString());
        }
        return conn;
    }
}
