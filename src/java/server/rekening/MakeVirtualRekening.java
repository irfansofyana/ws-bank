package server.rekening;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.*;

/**
 *
 * @author jakbar
 */
@WebService(serviceName = "MakeVirtualRekening")
public class MakeVirtualRekening {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs = null;
    ConnectionMySQL connectSQL = new ConnectionMySQL();

    public String format(int id) {
        String temp_id;

        if (id < 10) {
            temp_id = "000" + String.valueOf(id);
        } else if (id < 100) {
            temp_id = "00" + String.valueOf(id);
        } else if (id < 1000) {
            temp_id = "0" + String.valueOf(id);
        } else {
            temp_id = String.valueOf(id);
        }

        return temp_id;
    }

    @WebMethod(operationName = "makeVirtualRekening")
    public String makeVirtualRekening(
        @WebParam(name = "nomor_rekening") String rek
    ) {

        String rekening = "";

        try {
            conn = connectSQL.getConnect();
            pstm = conn.prepareStatement("SELECT id_rekening, "
                    + "count(nomor_virtual) "
                    + "FROM rekening JOIN virtual_rekening USING(id_rekening) "
                    + "WHERE nomor_rekening = " + rek
                    + " GROUP BY id_rekening");

            rs = pstm.executeQuery();
            int id = 0;
            int count = 0;

            while (rs.next()) {
                System.out.println(rs);
                id = rs.getInt("id_rekening");
                count = rs.getInt("count(nomor_virtual)") + 1;
            }

            String temp_id = format(id);
            String temp_count = format(count);

            rekening = "9876" + temp_id + temp_count;
            pstm = conn.prepareStatement("INSERT INTO virtual_rekening"
                    + "(id_rekening, nomor_virtual) "
                    + "VALUE (?,?)");
            pstm.setInt(1, id);
            pstm.setString(2, rekening);
            pstm.executeUpdate();

        } catch (Exception e) {
            System.out.println("Failed to Show :" + e.toString());
        }

        return rekening;
    }
}
