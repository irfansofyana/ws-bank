package server.rekening;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.*;

/**
 *
 * @author irfansofyana
 */
@WebService(serviceName = "CheckTransaksi")
public class CheckTransaksi {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs = null;
    ConnectionMySQL cmsql = new ConnectionMySQL();

    @WebMethod(operationName = "check")
    public boolean check(
        @WebParam(name = "nomor_rekening") String nrek,
        @WebParam(name = "nominal") int nominal,
        @WebParam(name = "start") String startD,
        @WebParam(name = "finish") String endD
    ) {

        boolean isTrue = false;

        try {
            conn = cmsql.getConnect();
            String querySQL = "SELECT COUNT(*) FROM transaksi WHERE nominal = "
                + String.valueOf(nominal)
                + " AND tujuan = '" + nrek
                + "' AND jenis = 'KREDIT'"
                + " AND waktuTransaksi > '" + startD
                + "' AND waktuTransaksi < '" + endD + "'";
            pstm = conn.prepareStatement(querySQL);
            rs = pstm.executeQuery();

            while (rs.next()) {
                if (rs.getInt(1) > 0) {
                    isTrue = true;
                }
            }

        } catch (Exception e) {
            System.out.println("Connection Fail" + e.toString());
        }

        return isTrue;
    }
}
