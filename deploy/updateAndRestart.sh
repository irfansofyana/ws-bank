#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/ubuntu/Project/ws-bank
sudo rm -rf /var/lib/tomcat9/webapps/web

cd Project

# clone the repo again
git clone git@gitlab.informatika.org:if3110-2019-02-k03-01/ws-bank.git

cd /home/ubuntu/Project/ws-bank

sudo cp -r build/web /var/lib/tomcat9/webapps