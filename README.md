# WS-Bank

## Deskripsi Web Service
Web service bank adalah service yang digunakan untuk Aplikasi Bank Pro dan Engima. Web service ini dibangun di atas
java servlet menggunakan JAX-WS dengan protokol SOAP. Layanan yang disediakan oleh web service ini adalah
- Validasi nomor rekening. Jika nomor rekening terdaftar di basis data, maka nomor tersebut valid
- Memberikan data rekening seorang nasabah. Data pengguna meliputi nama pengguna, saldo terakhir, dan riwayat transaksi(debit dan kredit)
- Melakukan transaksi transfer dengan input nomor rekening pengirim, nomor rekening/akun virtual penerima, dan jumlah uang yang ditransfer. Layanan
mengembalikan status transfer (berhasil/gagal). Transfer berhasil jika:
    1. Nomor rekening atau akun virtual tujuan valid
    2. Saldo rekening mencukupi untuk transaksi
  Jika transfer berhasil, akan dicatat transaksi debit pada rekening pengirim dan transaksi kredit pada rekening penerima.
- Membuat akun virtual untuk suatu nomor rekening. Layanan mengembalikan nomor unik akun virtual tersebut.
- Mengecek ada atau tidak sebuah transaksi kredit dalam suatu rentang waktu. Input yang diterima adalah nomor rekening atau akun virtual tujuan, jumlah nominal yang diharapkan, dan rentang waktu (start datetime, end datetime).


## Basis Data
WS Bank menggunakan basis data yang terdiri dari beberapa tabel berikut
- Bank, memiliki atribut id_bank dan name
- Rekening, memiliki atribut id_rekening, nama, nomor_rekening, saldo, dan id_bank
- Transaksi, memiliki atribut idTransaksi, idRekening, jenis, nominal, tujuan, dan waktuTransaksi
- Virtual Rekening, memiliki atribut id_virtual, id_rekening, dan nomor_virtual


## Pembagian Kerja
- Validasi rekening: 13517075
- Data rekening: 13517075
- Akun virtual: 13517075
- Pengecekan transaksi: 13517078
- Melakukan transfer: 13517057
- CI/CD: 13517075
- Eksplorasi dan setup mesin deployment: 13517075, 13517057

# URL
- URL deployment web service bank : http://ec2-18-212-188-140.compute-1.amazonaws.com:8080/web/